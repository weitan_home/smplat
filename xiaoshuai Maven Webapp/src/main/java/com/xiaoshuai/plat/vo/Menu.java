package com.xiaoshuai.plat.vo;
/**
 * 菜单
 * @author 宗潇帅
 * @修改日期 2014-7-14上午10:28:31
 */
public class Menu {
	private Button[] button;

	public Button[] getButton() {
		return button;
	}

	public void setButton(Button[] button) {
		this.button = button;
	}
	
}
