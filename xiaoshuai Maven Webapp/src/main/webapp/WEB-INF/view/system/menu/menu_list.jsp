<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/common/meta.jsp" />
<meta http-equiv="X-UA-Compatible" content="edge" />
<title>菜单管理</title>
	<script type="text/javascript">
	var dataGrid;
	$(function() {
		dataGrid = $('#dataGrid').datagrid({
			url : '${ctx}' + '/menu/list',
			rownumbers : true,
			pagination : true,
			checkOnSelect:true,
			showFooter: true,
			//onLoadSuccess:compute,
			idField : 'id',
			sortOrder : 'asc',
			pageSize : 50,
			pageList : [ 10, 20, 30, 40, 50],
			frozenColumns : [ [ {
				width : '80',
				title : '菜单ID',
				field : 'id',
				checkbox:true,
				sortable : true
			}, {
				width : '100',
				title : '菜单名称',
				field : 'menuname',
				sortable : true
			} , {
				width : '200',
				title : '菜单路径',
				field : 'url',
				sortable : true
			},{
				width : '150',
				title : '菜单图标',
				field : 'icon'
			},{
				width : '150',
				title : '数字',
				field : 'num'
			} , {
				field : 'action',
				title : '操作',
				width : 120,
				formatter : function(value, row, index) {
					return "开发中";
				}
			} ] ],
			toolbar : '#toolbar'
		});
		//增加查询的方法
		$("#doSearch").click(function(){
            doSearch();
        });
	});
		  function computes() {//计算函数
            var rows = $('#dataGrid').datagrid('getRows');//获取当前的数据行
            var ptotal = 0;//计算listprice的总和;//统计unitcost的总和
            for (var i = 0; i < rows.length; i++) {
                ptotal += rows[i]['num'];
            }
            //新增一行显示统计信息
            $('#dataGrid').datagrid('appendRow', { itemid: '<b>统计：</b>', num: ptotal});
        }
	function addFun() {
		parent.$.modalDialog({
			title : '添加',
			width : 500,
			height : 300,
			href : '${ctx}/role/addPage',
			buttons : [ {
				text : '添加',
				handler : function() {
					parent.$.modalDialog.openner_dataGrid = dataGrid;//因为添加成功之后，需要刷新这个treeGrid，所以先预定义好
					var f = parent.$.modalDialog.handler.find('#roleAddForm');
					f.submit();
				}
			} ]
		});
	}
	//2017年2月14日16:18:13 增加一个查询的demo
	function doSearch(){
        var name=$("#name").val();
        $("#dataGrid").datagrid('load',{  
            name:name
        }); //重新载入 
    }
	function deleteFun(id) {
		if (id == undefined) {//点击右键菜单才会触发这个
			var rows = dataGrid.datagrid('getSelections');
			id = rows[0].id;
		} else {//点击操作里面的删除图标会触发这个
			dataGrid.datagrid('unselectAll').datagrid('uncheckAll');
		}
		parent.$.messager.confirm('询问', '您是否要删除当前角色？', function(b) {
			if (b) {
				progressLoad();
				$.post('${ctx}/role/delete', {
					id : id
				}, function(result) {
					if (result.success) {
						parent.$.messager.alert('提示', result.msg, 'info');
						dataGrid.datagrid('reload');
					}
					progressClose();
				}, 'JSON');
			}
		});
	}
	
	function editFun() {
		var rows = dataGrid.datagrid('getSelections');
		if (rows == undefined||rows==""||rows.length<1) {
			alert("最少选择一条数据");
		}else if(rows.length>1){
			alert("最多选择一条数据");
		} else {
			$('#edit').show().dialog({
			    title: '修改',
			    width: 400,
			    height: 200,
			    href:'${ctx}/menu/goEdit/'+rows[0].id,
			    closed: false,
			    border:false,
			    cache: false,
			    modal: true
			});
		}
	}
	function editFunDialog() {
		alert("等待接入");
		}
	function grantFun(id) {
		if (id == undefined) {
			var rows = dataGrid.datagrid('getSelections');
			id = rows[0].id;
		} else {
			dataGrid.datagrid('unselectAll').datagrid('uncheckAll');
		}
		
		parent.$.modalDialog({
			title : '授权',
			width : 500,
			height : 500,
			href : '${ctx}/role/grantPage?id=' + id,
			buttons : [ {
				text : '授权',
				handler : function() {
					parent.$.modalDialog.openner_dataGrid = dataGrid;//因为添加成功之后，需要刷新这个dataGrid，所以先预定义好
					var f = parent.$.modalDialog.handler.find('#roleGrantForm');
					f.submit();
				}
			} ]
		});
	}
	
	</script>
</head>
<body class="easyui-layout" data-options="fit:true,border:false">
	<div data-options="region:'center',fit:true,border:false">
		<table id="dataGrid" data-options="fit:true,border:false"></table>
	</div>
	<div id="toolbar" style="display: none;">
					<form id="jsp_user_user_searchuserForm" method="post">
					<fieldset>
						<legend>筛选</legend>
						<table class="tableForm">
							<tr>
								<th>查询内容</th>
								<td>
									<input type="text" name="name" id="name">
								</td>
								<td> 
								<a href="javascript:void(0);" id="doSearch" class="easyui-linkbutton" data-options="iconCls:'icon-serach',plain:true">查询</a> 
							   </td>
							   	<td>
							 </td>
							</tr>
						</table>
					</fieldset>
				</form>
			<a onclick="alert('更新中');" href="javascript:void(0);" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-add'">添加</a>
			<a class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="editFun();"href="javascript:void(0);" style="float: left;" id="changepre" >编辑</a>
			<a class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="editFunDialog();"href="javascript:void(0);" style="float: left;" id="changepre" >当前页面弹出dialog编辑</a>
	</div>
	<div id="edit" style="display:none;">
		<form action="">
			菜单名称:<input type="text" class="easyui-text" name="menuname">			
		</form>
	</div>
</body>
</html>