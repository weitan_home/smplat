package com.xiaoshuai.plat.pojo;

import java.io.Serializable;
/**
 * 属性类
 * @author 小帅帅丶
 * @Title Attributes
 * @时间   2017-2-7下午4:09:43
 */
public class Attributes implements Serializable {
	private static final long serialVersionUID = 1L;
	private String url;

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
