SpringMVC+MongoDB+Maven整合
MongoDB使用是2.4.8的版本 放在百度云盘。
http://pan.baidu.com/s/1o7VaW9W
Menu表的sql文件，在附件里面下载
#基于SpringMVC+MongoDB数据库做的微信接入等一些常用接口的DEMO
2017年4月1日13:58:19
MongoDB增加用户名密码验证。在mongo-context.xml文件中有相关配置和说明，需要注意的是如果去掉_class字段的保存。验证用户名和密码，以及mongo:options是不可用的。如果不在乎。则可以用spring-data-mongo自带的usercred类进行用户名密码验证。
2017年1月19日09:50:13
增加了定时执行任务获取AccessToken的代码。spring.xml已经配置了相关标签。
使用注解方式实现定时执行。
只实现了回调 和oauth 接口
1. common存放相关基础代码和微信常量
1.1 com/xiaoshuai/plat/common/weixin/WXConstants.java 修改为自己的微信相关的APPID APPSERCET
1.2 com.xiaoshuai.plat.controller存放为请求访问层代码
1.2.1 WXConfigController.java 回调配置需要用到，GET为回调。POST 为用户发送信息进行处理并返回
1.2.2 WXOauthController.java Oauth授权获取用户信息的代码

2. com.xiaoshuai.plat.vo 存放微信相关的接口基础对象

3.com.xiaoshuai.plat.util 存放相关工具类代码包含微信需要用到的
3.1 com.xiaoshuai.plat.util.weixin 存放微信相关工具类

4.com.xiaoshuai.plat.service.weixin 存放微信用户给公众发送信息进行处理的方法， 上一级为操作数据的service 不保存不需要关注

5.com.xiaoshuai.plat.model.weixin 存微信的消息类型的对象，上一级为数据库的model  不保存不需要关注